/**
 * @lastModifiedBy Arpit Sharma
 * @email arpit.sharma@mtxb2b.com
 * @desc Returns filtered string
 */
 export function getFilteredString( dataString , searchKey, limit){
    return new Promise(
        (resolve, reject) => {
            try{
                let lowerCaseString = ' ' + dataString.toLowerCase();
                let lowerSearchKey = ' ' + searchKey.toLowerCase();
                let searchKeyIndex = lowerCaseString.indexOf(lowerSearchKey);
                let description;
                if(dataString.length <= limit){
                    description = dataString;
                }else if(searchKeyIndex != -1){
                    let startingIndex = lowerCaseString.lastIndexOf('.',searchKeyIndex) != -1 ? lowerCaseString.lastIndexOf('.',searchKeyIndex) : 0;
                    let endingIndex = startingIndex + limit < dataString.length ? startingIndex + limit : dataString.length;
                    if(startingIndex != 0){
                        description = '...' + dataString.slice(startingIndex,endingIndex - 6) + '...';
                    }else{
                        description = dataString.slice(startingIndex,endingIndex - 3) + '...';
                    }
                }else{
                    description =  dataString.slice(0,limit - 3)+'...';
                }
                resolve(description);
            }catch(error){
                reject(error);
            }
        }
    ); 
}